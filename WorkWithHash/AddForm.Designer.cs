﻿
namespace WorkWithHash
{
    partial class AddForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.AddID = new System.Windows.Forms.TextBox();
            this.Addfio = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.AddGroup = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Addcourse = new System.Windows.Forms.TextBox();
            this.AddDate = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID";
            // 
            // AddID
            // 
            this.AddID.Location = new System.Drawing.Point(120, 70);
            this.AddID.Name = "AddID";
            this.AddID.Size = new System.Drawing.Size(176, 26);
            this.AddID.TabIndex = 1;
            // 
            // Addfio
            // 
            this.Addfio.Location = new System.Drawing.Point(120, 114);
            this.Addfio.Name = "Addfio";
            this.Addfio.Size = new System.Drawing.Size(176, 26);
            this.Addfio.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "FIO";
            // 
            // AddGroup
            // 
            this.AddGroup.Location = new System.Drawing.Point(120, 205);
            this.AddGroup.Name = "AddGroup";
            this.AddGroup.Size = new System.Drawing.Size(176, 26);
            this.AddGroup.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 165);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 20);
            this.label3.TabIndex = 5;
            this.label3.Text = "Course";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 257);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 20);
            this.label4.TabIndex = 6;
            this.label4.Text = "Birthday";
            // 
            // Addcourse
            // 
            this.Addcourse.Location = new System.Drawing.Point(120, 159);
            this.Addcourse.Name = "Addcourse";
            this.Addcourse.Size = new System.Drawing.Size(176, 26);
            this.Addcourse.TabIndex = 7;
            // 
            // AddDate
            // 
            this.AddDate.Location = new System.Drawing.Point(120, 257);
            this.AddDate.Name = "AddDate";
            this.AddDate.Size = new System.Drawing.Size(176, 26);
            this.AddDate.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 208);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 20);
            this.label5.TabIndex = 9;
            this.label5.Text = "Group";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(111, 349);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(119, 38);
            this.button1.TabIndex = 10;
            this.button1.Text = "Add";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // AddForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(357, 399);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.AddDate);
            this.Controls.Add(this.Addcourse);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.AddGroup);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Addfio);
            this.Controls.Add(this.AddID);
            this.Controls.Add(this.label1);
            this.Name = "AddForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add student";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox AddID;
        private System.Windows.Forms.TextBox Addfio;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox AddGroup;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Addcourse;
        private System.Windows.Forms.DateTimePicker AddDate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
    }
}