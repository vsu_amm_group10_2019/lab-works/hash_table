﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WorkWithHash
{
    public partial class AddForm : Form
    {
        public Student Student { get; } = new Student();
        private FormState FormState;
        public AddForm(FormState formState, Student student = null)
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.FixedDialog;
            MinimizeBox = false;
            MaximizeBox = false;
            FormState = formState;
            switch (formState)
            {
                case FormState.ADD:
                    {
                        return;
                    }
                case FormState.EDIT:
                    {
                        AddID.ReadOnly = true;
                        AddID.Text = student.ID;
                        Addfio.Text = student.FIO;
                        Addcourse.Text = student.Course.ToString();
                        AddDate.Value = student.Birthday;
                        AddGroup.Text = student.Group;
                        Text = "Edit student";
                        break;
                    }
                case FormState.DELETE:
                   
                case FormState.SEARCH:
                    {
                        AddGroup.ReadOnly = true;
                        Addfio.ReadOnly = true;
                        AddDate.Enabled = false;
                        Addcourse.ReadOnly = true;
                        Text = "Search student";
                        break;
                    }

                case FormState.DISPLAY:
                    {
                        AddID.ReadOnly = true;
                        AddID.Text = student.ID;
                        Addfio.Text = student.FIO;
                        Addcourse.Text = student.Course.ToString();
                        AddDate.Value = student.Birthday;
                        AddGroup.Text = student.Group;

                        AddGroup.ReadOnly = true;
                        Addfio.ReadOnly = true;
                        AddDate.Enabled = false;
                        Addcourse.ReadOnly = true;
                        Text = "Show student";
                        break;
                    }
                    
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Student.ID = AddID.Text;
            if(FormState == FormState.ADD || FormState == FormState.EDIT)
            {
                Student.FIO = Addfio.Text;
                Student.Course = Convert.ToInt32(Addcourse.Text);
                Student.Group = AddGroup.Text;
                Student.Birthday = AddDate.Value;
            }
            Close();
        }
    }
}
