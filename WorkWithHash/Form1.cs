﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WorkWithHash
{
    public partial class Form1 : Form
    {
        private string FileName { get; set; }
        LineHashTable LineHash { get; set; } = new LineHashTable(100);
        
        public Form1()
        {
            InitializeComponent();
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddForm addForm = new AddForm(FormState.ADD);
            addForm.ShowDialog();
            LineHash.Add(addForm.Student);
            Redraw();
        }

        private void Redraw()
        {
            List<Student> students = LineHash.GetData();
            TableStudent.Rows.Clear();
            TableStudent.RowCount = students.Count;
            for(int i = 0; i < students.Count; i++)
            {
                TableStudent.Rows[i].Cells[0].Value = students[i].ID;
                TableStudent.Rows[i].Cells[1].Value = students[i].FIO;
                TableStudent.Rows[i].Cells[2].Value = students[i].Birthday;
                TableStudent.Rows[i].Cells[3].Value = students[i].Course;
                TableStudent.Rows[i].Cells[4].Value = students[i].Group;
            }
        }

        private void editToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AddForm search = new AddForm(FormState.SEARCH);
            search.ShowDialog();
            string id = search.Student.ID;
            Student student = LineHash.Find(id);
            if(student != null)
            {
                AddForm edit = new AddForm(FormState.EDIT, student);
                edit.ShowDialog();
                LineHash.Delete(id);
                LineHash.Add(edit.Student);
                Redraw();
            } else
            {
                MessageBox.Show("Student not found");
            }
            

        }

        private void searchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddForm search = new AddForm(FormState.SEARCH);
            search.ShowDialog();
            string id = search.Student.ID;
            Student student = LineHash.Find(id);
            if (student != null)
            {
                AddForm show = new AddForm(FormState.DISPLAY, student);
                show.ShowDialog();
               
            }
            else
            {
                MessageBox.Show("Student not found");
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddForm search = new AddForm(FormState.SEARCH);
            search.ShowDialog();
            string id = search.Student.ID;
            LineHash.Delete(id);
            Redraw();
        }

        private void newFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(FileName))
            {
                SaveToFile();
            }
            FileName = "";
            LineHash.Clear();
            Redraw();
        }

        private void saveFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(FileName))
            {
                SaveToFile();
            }
            else
            {
                saveAsToolStripMenuItem_Click(sender, e);
            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog.Filter = "Json File|*.json";
            saveFileDialog.ShowDialog();
            if (!string.IsNullOrEmpty(saveFileDialog.FileName))
            {
                FileName = saveFileDialog.FileName;
                SaveToFile();
            }
        }
        private void SaveToFile()
        {
            List<Student> students = LineHash.GetData();
            string result = JsonConvert.SerializeObject(students);
            File.WriteAllText(FileName, result);
        }

        private void openFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(FileName))
            {
                SaveToFile();
            }
            openFileDialog.Filter = "Json File|*.json";
            openFileDialog.ShowDialog();
            if (!string.IsNullOrEmpty(openFileDialog.FileName))
            {
                FileName = openFileDialog.FileName;
                string txt = File.ReadAllText(FileName);
                List<Student> students = JsonConvert.DeserializeObject<List<Student>>(txt);
                LineHash.Clear();
                foreach (Student student in students)
                {
                    LineHash.Add(student);
                }
                Redraw();
            }
        }
    }
}
