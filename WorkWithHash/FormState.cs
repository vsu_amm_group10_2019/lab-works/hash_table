﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkWithHash
{
    public enum FormState
    {
      ADD,
      EDIT,
      SEARCH,
      DELETE,
      DISPLAY
    }
}
