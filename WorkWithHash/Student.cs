﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkWithHash
{
    public class Student
    {
        public string ID { get; set; }
        public string FIO { get; set; }
        public DateTime Birthday { get; set; }
        public int Course { get; set; }
        public string Group { get; set; }
        public static int Hash(string id)
        {
            int result = 0;
            foreach (char c in id)
            {
                result += c;
            }
            return result;
        }

    }
}
